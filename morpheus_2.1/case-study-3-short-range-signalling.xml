<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Details>Case study 3:

Comparing individual-based approaches to modelling the self-organization of multicellular tissues
Osborne et al., 2016, BioRvix.
http://biorxiv.org/content/biorxiv/early/2016/09/09/074351.full.pdf
</Details>
        <Title>CaseStudy3-Short-Range-Signalling</Title>
    </Description>
    <Global>
        <Constant symbol="p_div" value="0.10"/>
        <Constant symbol="R_S" value="45.0" name="Removal zone radius"/>
        <Constant symbol="R_P" value="15.0" name="Proliferation zone radius"/>
        <Constant symbol="t_min" value="0.0"/>
        <Constant symbol="D" value="0.0" name="Delta"/>
        <!--    <Disabled>
        <Constant symbol="hetero_ss" value="0.0"/>
    </Disabled>
-->
        <!--    <Disabled>
        <Constant symbol="distance" value="0.0"/>
    </Disabled>
-->
    </Global>
    <Space>
        <Lattice class="square">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
            <Size symbol="size" value="100,100,0"/>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="500"/>
        <TimeSymbol symbol="time"/>
    </Time>
    <CellTypes>
        <CellType class="biological" name="cells">
            <VolumeConstraint target="16" strength="0.1"/>
            <SurfaceConstraint target="0.9" mode="aspherity" strength="0.1"/>
            <CellDeath name="remove cell outside of radius">
                <Condition>sqrt((cell.center.x-size.x/2)^2+(cell.center.y-size.y/2)^2 ) 
  > R_S</Condition>
            </CellDeath>
            <CellDivision name="Delta-dependent proliferation" division-plane="major">
                <Condition>time > 20 
and age > t_min 
and sqrt((cell.center.x-size.x/2)^2+(cell.center.y-size.y/2)^2 ) &lt; R_P
and rand_uni(0,1) &lt; p_div*dT
</Condition>
                <Triggers/>
            </CellDivision>
            <Property symbol="D" value="0" name="Delta"/>
            <Property symbol="Dn" value="0" name="Delta neighbors"/>
            <Property symbol="N" value="0" name="Notch"/>
            <NeighborhoodReporter name="Average Delta on neighbors">
                <Input scaling="length" value="D" noflux-cell-medium="true"/>
                <Output symbol-ref="Dn" mapping="average"/>
            </NeighborhoodReporter>
            <System solver="adaptive45-ck" name="Notch signalling model">
                <DiffEqn symbol-ref="N">
                    <Expression>if(time &lt; 10, 0, Dn^n/(k_N+Dn^n)-N)</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="D">
                    <Expression>if(time &lt; 10, 0, r_DN*(k_D/(k_D+N^n)-D))</Expression>
                </DiffEqn>
                <Constant symbol="r_DN" value="1"/>
                <Constant symbol="k_D" value="0.01"/>
                <Constant symbol="k_N" value="0.01"/>
                <Constant symbol="n" value="2"/>
            </System>
            <Property symbol="age" value="0"/>
            <System solver="euler" time-step="0.05" name="update cell age">
                <Rule symbol-ref="age">
                    <Expression>age+dT</Expression>
                </Rule>
            </System>
            <!--    <Disabled>
        <Function symbol="hetero_ss">
            <Expression>if( D>0.25 or Dn>0.25, 1, 0)</Expression>
        </Function>
    </Disabled>
-->
            <!--    <Disabled>
        <Function symbol="distance">
            <Expression>sqrt((cell.center.x-size.x/2)^2+(cell.center.y-size.y/2)^2)</Expression>
        </Function>
    </Disabled>
-->
            <Property symbol="hetero_ss" value="0.0"/>
            <Property symbol="distance" value="0.0"/>
            <Equation symbol-ref="hetero_ss">
                <Expression>if( D>0.25 or Dn>0.25, 1, 0)</Expression>
            </Equation>
            <Equation symbol-ref="distance">
                <Expression>sqrt((cell.center.x-size.x/2)^2+(cell.center.y-size.y/2)^2)</Expression>
            </Equation>
        </CellType>
    </CellTypes>
    <CPM>
        <Interaction>
            <Contact type1="cells" type2="cells" value="-0.1"/>
            <Contact type1="cells" type2="cells" value="-0.2"/>
        </Interaction>
        <MonteCarloSampler stepper="edgelist">
            <MetropolisKinetics temperature="0.1"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
            <MCSDuration symbol="dT" value="0.01"/>
        </MonteCarloSampler>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </ShapeSurface>
    </CPM>
    <CellPopulations>
        <Population size="0" type="cells">
            <InitCircle mode="regular" number-of-cells="450">
                <Dimensions radius="R_S" center="size.x/2, size.y/2, 0"/>
            </InitCircle>
            <InitProperty symbol-ref="D">
                <Expression>rand_uni(0,1)</Expression>
            </InitProperty>
            <InitProperty symbol-ref="N">
                <Expression>rand_uni(0,1)</Expression>
            </InitProperty>
        </Population>
    </CellPopulations>
    <Analysis>
        <Gnuplotter time-step="5">
            <Plot>
                <Cells value="D" min="0.0" max="1">
                    <ColorMap>
                        <Color value="1.0" color="red"/>
                        <Color value="0.5" color="white"/>
                        <Color value="0.0" color="blue"/>
                    </ColorMap>
                </Cells>
            </Plot>
            <Terminal name="png"/>
        </Gnuplotter>
        <Logger time-step="5">
            <Input>
                <Symbol symbol-ref="hetero_ss"/>
                <Symbol symbol-ref="distance"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <!--    <Disabled>
        <Plots>
            <Plot>
                <Style style="points"/>
                <Terminal terminal="png"/>
                <X-axis>
                    <Symbol symbol-ref="distance"/>
                </X-axis>
                <Y-axis>
                    <Symbol symbol-ref="hetero_ss"/>
                </Y-axis>
                <Range>
                    <Time mode="since last plot"/>
                </Range>
            </Plot>
        </Plots>
    </Disabled>
-->
        </Logger>
    </Analysis>
</MorpheusModel>
