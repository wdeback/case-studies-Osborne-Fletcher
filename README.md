Case studies
-------------

In [Osborne et al., PLoS Comp Biol, 2017](http://dx.doi.org/10.1371/journal.pcbi.1005387), the authors present four case studies to compare different cell-based modeling approaches.

- *Sorting*: cell sorting due to differential adhesion
- *Proliferation, death and differentiation*: simplified version of intestinal crypt model
- *Short-range signalling*: Notch-mediated lateral inhibition model with proliferation
- *Long-range signalling*: transcytosis-mediated gradient regulating tissue growth as in wing disc

In the paper, these case studies were implemented in 5 different cell-based models (cellular automata, cellular Potts model, overlapping spheres, voronoi tesselation and vertex model) using the [Chaste framework](https://www.cs.ox.ac.uk/chaste/cell_based_index.html).
Subsequently, simulations are performed and results are compared.

To reproduce the original simulations, see [this Chaste tutorial](https://chaste.cs.ox.ac.uk/trac/wiki/PaperTutorials/CellBasedComparison2016).

Reproduction in Morpheus
------------

Using the descriptions and tables of parameters in the paper, I re-implemented the cellular Potts model version of all 4 case studies in [Morpheus](https://imc.zih.tu-dresden.de//wiki/morpheus) ([git repo](https://gitlab.com/morpheus.lab/morpheus)).

The resulting models in MorpheusML format are presented here (XML files), together with videos of typical simulations (MP4 files). 


Simulation
----------

To simulate these models, simply load the XML file in a recent version of [Morpheus](https://imc.zih.tu-dresden.de//wiki/morpheus) ([git repo](https://gitlab.com/morpheus.lab/morpheus)). 
The XML files contain full specification of the simulation models, including initial conditions, visualization and data export. 

Reference
---------

Comparing individual-based approaches to modelling the self-organization of multicellular tissues

James M. Osborne, Alexander G. Fletcher, Joe M. Pitt-Francis, Philip K. Maini, David J. Gavaghan

PLoS Comp Biol, 2017. DOI: [10.1371/journal.pcbi.1005387](http://dx.doi.org/10.1371/journal.pcbi.1005387)
